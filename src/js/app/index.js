import React, { Component } from 'react';
import Wheels from '../../components/Wheels.js';
import PlayButton from '../../components/Playbutton.js';


class App extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   winner: null,
  }
  // this.handleClick = this.handleClick.bind(this);

  // handleClick() {
  //   const wheel1 = Math.floor(Math.random() * 3 );
  // here is where the random number should be assigned to the wheel
  //   const wheel2 = Math.floor(Math.random() * 3 );
  //
  //   const wheel3 = Math.floor(Math.random() * 3 );
  //
  // after assigning the number to the wheels I would need to have a function to
  // check the number and change the colour of the wheel accordingly, now I have
  // commented this out in the PlayButton component in the inline styling. Then I
  // would check if all of the numbers in the wheels are the same and then change
  // state to winner: true and alert "YOU WIN!"
  // }
  render() {
    return (
      <div className='hero'>
        <h1>FRUIT MACHINE</h1>
        <div className='wheels'>
          <Wheels />
        </div>
        <PlayButton
          handleClick={ this.handleClick }
        />
      </div>
    );
  }
}

export default App;
