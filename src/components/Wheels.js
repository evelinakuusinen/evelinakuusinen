import React from 'react';

const Wheels = () => {
  const wheelStyle = {
    border: '2px solid black',
    fontSize: '20px',
    marginBottom: '5px',
    padding: '8px 10px 10px 10px',
    height: '100px',
    width: '100px',
    // backgroundColor:
    // if spinresult === 0 ? 'red',
    // if else
    // // spinresult === 1 ? 'blue'
    // if else // spinresult === 2 ? 'green' ||
    // if else // spinresult === 3 ? 'yellow'
    // else 'white',
  };

  return (
    <div>
      {/* <div style={ wheelStyle } > {this.state.color || 'white'} </div> */}
      <div className='wheel1' style={ wheelStyle } />
      <div className='wheel2' style={ wheelStyle } />
      <div className='wheel3' style={ wheelStyle } />
    </div>
  );
};

export default Wheels;
