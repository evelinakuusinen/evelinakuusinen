import React from 'react';

const PlayButton = () => {
  const buttonStyle = {
    border: '2px solid black',
    backgroundColor: 'white',
    marginRight: '5px',
    fontSize: '14px',
    padding: '8px 10px',
  };

  return (
    <div>
      <button style={ buttonStyle } >
        {/* would add onClick={ this.handleClick } to the button but removed it to
        not get errors when running on localhost */}
        PLAY
      </button>
    </div>
  );
};

export default PlayButton;
